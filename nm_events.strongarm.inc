<?php
/**
 * @file
 * nm_events.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function nm_events_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'additional_settings__active_tab_nm_event';
  $strongarm->value = 'edit-taxonomy-menu-trails';
  $export['additional_settings__active_tab_nm_event'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addressfield_staticmap_api_0';
  $strongarm->value = 'google_maps';
  $export['addressfield_staticmap_api_0'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addressfield_staticmap_api_1';
  $strongarm->value = 'google_maps';
  $export['addressfield_staticmap_api_1'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addressfield_staticmap_api_key_0';
  $strongarm->value = '';
  $export['addressfield_staticmap_api_key_0'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addressfield_staticmap_api_key_1';
  $strongarm->value = '';
  $export['addressfield_staticmap_api_key_1'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addressfield_staticmap_directions_link_0';
  $strongarm->value = 0;
  $export['addressfield_staticmap_directions_link_0'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addressfield_staticmap_directions_link_1';
  $strongarm->value = 1;
  $export['addressfield_staticmap_directions_link_1'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addressfield_staticmap_directions_text_0';
  $strongarm->value = 'Get Directions';
  $export['addressfield_staticmap_directions_text_0'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addressfield_staticmap_directions_text_1';
  $strongarm->value = 'Get Directions';
  $export['addressfield_staticmap_directions_text_1'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addressfield_staticmap_field_kml';
  $strongarm->value = array();
  $export['addressfield_staticmap_field_kml'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addressfield_staticmap_field_names';
  $strongarm->value = array(
    'field_nm_address' => 'field_nm_address',
  );
  $export['addressfield_staticmap_field_names'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addressfield_staticmap_gmap_icon_url_0';
  $strongarm->value = '';
  $export['addressfield_staticmap_gmap_icon_url_0'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addressfield_staticmap_gmap_icon_url_1';
  $strongarm->value = '';
  $export['addressfield_staticmap_gmap_icon_url_1'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addressfield_staticmap_gmap_link_0';
  $strongarm->value = 1;
  $export['addressfield_staticmap_gmap_link_0'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addressfield_staticmap_gmap_link_1';
  $strongarm->value = 1;
  $export['addressfield_staticmap_gmap_link_1'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addressfield_staticmap_gmap_link_target_0';
  $strongarm->value = '_blank';
  $export['addressfield_staticmap_gmap_link_target_0'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addressfield_staticmap_gmap_link_target_1';
  $strongarm->value = '_blank';
  $export['addressfield_staticmap_gmap_link_target_1'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addressfield_staticmap_gmap_size_0';
  $strongarm->value = '600x600';
  $export['addressfield_staticmap_gmap_size_0'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addressfield_staticmap_gmap_size_1';
  $strongarm->value = '280x280';
  $export['addressfield_staticmap_gmap_size_1'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addressfield_staticmap_gmap_type_0';
  $strongarm->value = 'roadmap';
  $export['addressfield_staticmap_gmap_type_0'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addressfield_staticmap_gmap_type_1';
  $strongarm->value = 'roadmap';
  $export['addressfield_staticmap_gmap_type_1'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addressfield_staticmap_gmap_zoom_0';
  $strongarm->value = '14';
  $export['addressfield_staticmap_gmap_zoom_0'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addressfield_staticmap_gmap_zoom_1';
  $strongarm->value = '14';
  $export['addressfield_staticmap_gmap_zoom_1'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_nm_event';
  $strongarm->value = 0;
  $export['comment_anonymous_nm_event'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_nm_event';
  $strongarm->value = 1;
  $export['comment_default_mode_nm_event'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_nm_event';
  $strongarm->value = '50';
  $export['comment_default_per_page_nm_event'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_nm_event';
  $strongarm->value = 1;
  $export['comment_form_location_nm_event'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_nm_event';
  $strongarm->value = '0';
  $export['comment_nm_event'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_nm_event';
  $strongarm->value = '1';
  $export['comment_preview_nm_event'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_nm_event';
  $strongarm->value = 1;
  $export['comment_subject_field_nm_event'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'enable_revisions_page_nm_event';
  $strongarm->value = 1;
  $export['enable_revisions_page_nm_event'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__nm_event';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'search_result' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'nm_event_listing' => array(
        'custom_settings' => TRUE,
      ),
      'nm_embed_gallery' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'title' => array(
          'weight' => '0',
        ),
        'path' => array(
          'weight' => '1',
        ),
        'metatags' => array(
          'weight' => '40',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__nm_event'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_nm_event';
  $strongarm->value = array();
  $export['menu_options_nm_event'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_nm_event';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_nm_event'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_nm_event';
  $strongarm->value = array(
    0 => 'status',
    1 => 'revision',
  );
  $export['node_options_nm_event'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_nm_event';
  $strongarm->value = '0';
  $export['node_preview_nm_event'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_nm_event';
  $strongarm->value = 0;
  $export['node_submitted_nm_event'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_nm_event_pattern';
  $strongarm->value = 'events/[node:field_nm_date:custom:Y]-[node:field_nm_date:custom:m]-[node:field_nm_date:custom:d]/[node:title]';
  $export['pathauto_node_nm_event_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_nm_event_category_pattern';
  $strongarm->value = 'events/[term:name]';
  $export['pathauto_taxonomy_term_nm_event_category_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'save_continue_nm_event';
  $strongarm->value = 'Save and add fields';
  $export['save_continue_nm_event'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_diff_inline_nm_event';
  $strongarm->value = 0;
  $export['show_diff_inline_nm_event'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_preview_changes_nm_event';
  $strongarm->value = 0;
  $export['show_preview_changes_nm_event'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'taxonomy_menu_trails_node_nm_event';
  $strongarm->value = array(
    'selection_method' => 'first',
    'only_without_menu' => 0,
    'set_breadcrumb' => '0',
    'term_path' => 'default',
    'term_path_patterns' => array(),
    'instances' => array(
      'field_nm_event_category' => 'field_nm_event_category',
    ),
    'paths_ui' => '',
  );
  $export['taxonomy_menu_trails_node_nm_event'] = $strongarm;

  return $export;
}
